const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "test_DB",
});

// !check db connection
db.connect((err) => {
  if (err) {
    throw err;
  } else {
    console.log("success");
  }
});

app.post("/getpermission_name", (req, res) => {
  // * catch variable values from the frontend
    // console.log(req.body);
  const pCategory = req.body.pCategory;

  db.query(
    "SELECT * FROM permission_type WHERE permission_category=?",
    [pCategory],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
           console.log('test'+result);
      }
    }
  );
});

app.get("/getdata", (req, res) => {
  db.query("SELECT * FROM user", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.get("/getchannel", (req, res) => {
  db.query("SELECT * FROM channel", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

app.get("/getrole", (req, res) => {
  db.query("SELECT * FROM role", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});
app.post("/getUserRole", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const iduser = req.body.idVal;

  db.query(
    // "SELECT role.role_id, role.role_name FROM user_role INNER JOIN role ON role.role_id = user_role.role_id WHERE user_role.user_id = ?",
    "SELECT role.role_id,role.role_name FROM user_role INNER JOIN role ON role.role_id = user_role.role_id WHERE user_role.user_id = ?",
    [iduser],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});
app.get("/getpermission", (req, res) => {
  db.query(
    "SELECT permission_category FROM permission_type GROUP BY permission_category",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});
app.post("/getdataUser", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const iduser = req.body.idVal;

  db.query(
    "SELECT user.iduser, user.fn,user.ln, user.email,user.password,user.status,channel.channel_name,channel.idchannel FROM user_channel INNER JOIN user ON user.iduser = user_channel.user_id INNER JOIN channel ON user_channel.channel_id = channel.idchannel WHERE user.iduser = ?",
    [iduser],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});

app.post("/insertUser", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);
  const fn = req.body.fn;
  const ln = req.body.ln;
  const status = req.body.status;
  const channel = req.body.channel;
  const email = req.body.email;
  const password = req.body.password;
  const role = req.body.role;

  var sql = "INSERT INTO user(fn,ln,password,email,status) VALUES ?";
  var values = [[fn, ln, password, email, status]];
  db.query(sql, [values], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.insertId);
    db.query(
      "INSERT INTO user_channel(channel_id,user_id,status,date) VALUES (?,?,?,?)",
      [channel, result.insertId, status, "000/00/00"],

      (err, result2) => {
        if (err) {
          console.log(err);
        } else {
          res.send("inserted");
          role.forEach((role) => {
            const sql =
              "INSERT INTO user_role (role_id,user_id,status,date) VALUES (?,?,?,?)";
            db.query(
              sql,
              [role, result.insertId, status, "000000"],
              (error, result3) => {
                if (error) {
                  console.log(error.message);
                } else {
                  console.log(result3);
                }
              }
            );
          });
        }
      }
    );
  });
});

app.post("/updateUser", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const iduser = req.body.idVal;
  const fn = req.body.fn;
  const ln = req.body.ln;
  const status = req.body.status;
  const channel = req.body.channel;
  const email = req.body.email;
  const password = req.body.password;
  const role = req.body.role;

  var sql =
    "UPDATE user SET fn=?,ln=?,password=?,email=?,status=? WHERE iduser=?";
  var values = [fn, ln, password, email, status, iduser];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
    console.log("updated");
    db.query(
      "UPDATE user_channel SET channel_id=?,date=? WHERE user_id =?",
      [channel, "111/11/11", iduser],

      (err, result2) => {
        if (err) {
          console.log(err);
        } else {
          res.send("inserted");
          var sql = "DELETE FROM user_role WHERE user_id = ?";
          var val = [iduser];
          db.query(sql, val, function (err, result) {
            if (err) throw err;
            console.log("Number of records deleted: " + result.affectedRows);

            role.forEach((role) => {
              const sql =
                "INSERT INTO user_role (role_id,user_id,status,date) VALUES (?,?,?,?)";
              db.query(
                sql,
                [role, iduser, status, "111111"],
                (error, result3) => {
                  if (error) {
                    console.log(error.message);
                  } else {
                    console.log(result3);
                  }
                }
              );
            });
          });
        }
      }
    );
  });
});

app.post("/insertRole", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);
  const roleN = req.body.role;
  const info = req.body.info;
  const status = req.body.status;

  var sql =
    "INSERT INTO role(role_name,role_date,role_status,role_desc) VALUES ?";
  var values = [[roleN, "00000", status, info]];
  db.query(sql, [values], function (err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.insertId);
  });
});
app.post("/getdataRole", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const roleid = req.body.RoleidVal;

  db.query("SELECT * FROM role WHERE role_id = ?", [roleid], (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
      console.log(result);
    }
  });
});

app.post("/updateRole", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const Rid = req.body.Rid;
  const Rname = req.body.Rname;
  const Rdesc = req.body.Rdesc;
  const Rstatus = req.body.status;

  var sql =
    "UPDATE role SET role_name=?,role_desc=?,role_date=?,role_status=? WHERE role_id=?";
  var values = [Rname, Rdesc, "1111/11/11", Rstatus, Rid];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
    console.log("updated");
  });
});

app.post("/assignPermission", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const roleID = req.body.roleID;
  const PermissionID = req.body.PermissionID;
  const dateTime = new Date();
  PermissionID.forEach((PermissionID) => {
    const sql =
      "INSERT INTO role_permission (prole_id,permission_id,pstatus,pdate) VALUES (?,?,?,?)";
    db.query(
      sql,
      [roleID, PermissionID, "Active", dateTime],
      (error, result3) => {
        if (error) {
          console.log(error.message);
        } else {
          console.log(result3);
        }
      }
    );
  });
});
app.post("/insertPermission", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const newCategory = req.body.newCategory;
  const newPermission = req.body.newPermission;
  const dateTime = new Date();

  newPermission.forEach((newPermission) => {
    const sql =
      "INSERT INTO permission_type (permission_category,permission_name,permission_status,permission_date) VALUES (?,?,?,?)";
    db.query(
      sql,
      [newCategory, newPermission, "Active", dateTime],
      (error, result3) => {
        if (error) {
          console.log(error.message);
        } else {
          console.log(result3);
        }
      }
    );
  });
});


app.post("/updatePermission", (req, res) => {
    console.log(req.body);

    const Category = req.body.Category;
    const Permission = req.body.Permission;
    const dateTime = new Date();

  var sql = "DELETE FROM permission_type WHERE permission_category = ?";
  var val = [Category];
  db.query(sql, val, function (err, result) {
    if (err) throw err;
    console.log("Number of records deleted: " + result.affectedRows);

    Permission.forEach((Permission) => {
      const sql =
        "INSERT INTO permission_type (permission_category,permission_name,permission_status,permission_date) VALUES (?,?,?,?)";
    db.query(
      sql,
      [Category, Permission, "Active", dateTime],
      (error, result3) => {
        if (error) {
          console.log(error.message);
        } else {
          console.log(result3);
        }
      }
    );
    });
  });
});




 
app.post("/deleteRoleMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const dateTime = new Date();

     deleteID.forEach((deleteID) => {
       const sql = "UPDATE role SET role_status=?,role_date=? WHERE role_id=?";
       db.query(sql, ["Inactive", dateTime, deleteID], (error, result3) => {
         if (error) {
           console.log(error.message);
         } else {
           console.log(result3);
         }
       });
     });
});
 

app.post("/deleteUserMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
 

  deleteID.forEach((deleteID) => {
    const sql = "UPDATE user SET status=? WHERE iduser=?";
    db.query(sql, ["Inactive", deleteID], (error, result3) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log(result3);
      }
    });
  });
});

app.post("/deleteChannelMulti", (req, res) => {
  console.log(req.body);

  const deleteID = req.body.deleteID;
  const dateTime = new Date();

  deleteID.forEach((deleteID) => {
    const sql =
      "UPDATE channel SET channel_status=?,channel_date=? WHERE idchannel=?";
    db.query(sql, ["Inactive", dateTime, deleteID], (error, result3) => {
      if (error) {
        console.log(error.message);
      } else {
        console.log(result3);
      }
    });
  });
});
 app.post("/insertChannel", (req, res) => {
   // * catch variable values from frontend
   console.log(req.body);
   const channelN = req.body.channel;
   const info = req.body.info;
   const status = req.body.status;
   const dateTime = new Date();
   

   var sql =
     "INSERT INTO channel(channel_name,channel_date,channel_status,channel_desc) VALUES ?";
   var values = [[channelN, dateTime, status, info]];
   db.query(sql, [values], function (err, result) {
     if (err) throw err;
     console.log("Number of records inserted: " + result.insertId);
   });
 });



app.post("/getdataChannel", (req, res) => {
  // * catch variable values from frontend
  // console.log(req.body);
  const channelid = req.body.ChannelidVal;

  db.query(
    "SELECT * FROM channel WHERE idchannel = ?",
    [channelid],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
        console.log(result);
      }
    }
  );
});


app.post("/updateChannel", (req, res) => {
  // * catch variable values from frontend
  console.log(req.body);

  const Cid = req.body.Cid;
  const Cname = req.body.Cname;
  const Cdesc = req.body.Cdesc;
  const Cstatus = req.body.status;
  const dateTime = new Date();
  

  var sql =
    "UPDATE channel SET channel_name=?,channel_desc=?,channel_date=?,channel_status=? WHERE idchannel=?";
  var values = [Cname, Cdesc, dateTime, Cstatus, Cid];
  db.query(sql, values, function (err, result) {
    if (err) throw err;
    console.log("updated");
  });
});


// //* ..................
app.listen(3001, () => {
  console.log("running on port 3001");
});
