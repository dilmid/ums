import React, { useState, useEffect } from "react";
import {
  
  SelectPicker,
  Form,
  Modal,
  TagInput,
  Row,
  Col,
  Grid,
} from "rsuite";
import Axios from "axios";
 
export default function PermissionUpdate() {
  const [pCategory, setpCategory] = useState([]);
  const [pName, setpName] = useState([]);
  const [categoryVal, setcategoryVal] = useState("");
  const [state, setstate] = useState([])

  const resetBtn2 = () => {
    setcategoryVal([]);
    setstate([]);
  };
  useEffect(() => {
    Axios.get("http://localhost:3001/getpermission").then((response) => {
      setpCategory(response.data);
    });
  }, []);
    
   
  const showPname = (e) => {
    setcategoryVal(e);

    Axios.post("http://localhost:3001/getpermission_name", {
      pCategory: e,
    }).then((response) => {
      setpName(response.data);

      
    });
     
    
  };
      useEffect(() => {
        const map = Array.prototype.map;
        let newName = [];
        newName = map.call(pName, (eachLetter) => {
          return `${eachLetter.permission_name}`;
        });
        setstate(newName);
        console.log(state);
      }, [pName]);
 
  // const testbtn = () => {
  //        const map = Array.prototype.map;
  //    let newName = [];
  //    newName = map.call(pName, (eachLetter) => {
  //      return `${eachLetter.permission_name}`;
  //    });
  //    setstate(newName);
  //    console.log(state);
  // }

  const updatePermission = () => {
  Axios.post("http://localhost:3001/updatePermission", {
    Category: categoryVal,
    Permission: state,
  }).then(() => {
    console.log(" permission update success");
  });
}


  return (
    <React.Fragment>
      <Modal.Body className="permissionModal">
        <Grid fluid="true">
        
            <Form fluid="true">
             
                <Row className="show-grid">
                 
                    <Form.Group controlId="name-1" className="m-2">
                      <Form.ControlLabel>Category</Form.ControlLabel>

                      <SelectPicker
                        data={pCategory.map((val) => {
                          return {
                            label: val.permission_category,
                            value: val.permission_category,
                          };
                        })}
                        block
                        onChange={showPname}
                        value={categoryVal}
                      />
                    </Form.Group>
                 
                  {/* <Col xs={1}>
                    <Form.Group controlId="name-1" className="m-2">
                      <input
                        type="button"
                        className="greenBtn"
                        value="Search"
                        hidden={searchBtnShow}
                        onClick={testbtn}
                      />
                 
                    </Form.Group>
                  </Col> */}
                </Row>
             
 <Row className="show-grid">
                 
              <Form.Group controlId="email-1" className="m-2">
                <Form.ControlLabel>Permission(s)</Form.ControlLabel>
                <TagInput
                  block
                  value={state}
                  onChange={(event) => {
                    setstate(event);
                  }}
                  //   defaultValue={[`${names}`]}
                  //   defaultValue={[`${newName}`]}
                  defaultValue={state.map((val, kk) => `${val}`)}
                  //   defaultValue={[
                  //     pName
                  //       .reduce(
                  //         (acc, val) => `${acc}${val.permission_name} |`,
                  //         ""
                  //       )
                  //       .split(""),
                  //   ]}
                  trigger={["Enter", "Space", "Comma"]}
                />
              </Form.Group>
           
          
          </Row> </Form>
        </Grid>
      </Modal.Body>
      <Modal.Footer>
        <Row className="show-grid">
          <Col xs={5}>
            <div
              className="btnGroupmodal"
              style={{ margin: "0px", padding: "0px" }}
            >
              <input
                type="button"
                value="Submit"
                onClick={updatePermission}
                className="button-61 marginRIght"
              />
              <input
                type="button"
                value="Reset"
                onClick={resetBtn2}
                className="Cancelbtn marginRIght"
              />
            </div>
          </Col>
        </Row>
      </Modal.Footer>
    </React.Fragment>
  );
}
