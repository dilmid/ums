// import { Tooltip, Input, Whisper } from "rsuite";
import { faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../styles/pswValidation.css";
export default function PswValidation() {
  return (
    <div className="container">
      <p id="capital">
        <FontAwesomeIcon className="fa_times icon" icon={faTimes} />
        <FontAwesomeIcon className="fa_check icon" icon={faCheck} />
        <span>Upper Case</span>
      </p>
      <p id="lower">
        <FontAwesomeIcon className="fa_times icon" icon={faTimes} />
        <FontAwesomeIcon className="fa_check icon" icon={faCheck} />
        <span>Lower Case</span>
      </p>
      <p id="chars">
        <FontAwesomeIcon className="fa_times icon" icon={faTimes} />
        <FontAwesomeIcon className="fa_check icon" icon={faCheck} />
        <span>characters</span>
      </p>
      <p id="numbers">
        <FontAwesomeIcon className="fa_times icon" icon={faTimes} />
        <FontAwesomeIcon className="fa_check icon" icon={faCheck} />
        <span>use numbers</span>
      </p>
      <p id="more8">
        <FontAwesomeIcon className="fa_times icon" icon={faTimes} />
        <FontAwesomeIcon className="fa_check icon" icon={faCheck} />
        <span>8+ characters</span>
      </p>
    </div>
  );

  // return (
  //   <Whisper
  //     trigger="focus"
  //     speaker={
  //       <Tooltip>
  //         <MsgBox />
  //       </Tooltip>
  //     }
  //   >
  //     <Input
  //       style={{ width: 300 }}
  //       placeholder="Default Input"
  //       onChange={checkValidate}
  //     />
  //   </Whisper>
  // );
}
