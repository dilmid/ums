// import React from "react";
import {
  Modal,
 
  Panel,
 
  Grid,
  Row,
  // Button,
  Tooltip,
  Input,
  Whisper,
  CheckPicker,
  Col,
  InputGroup,
  SelectPicker,
  Form,
} from "rsuite";

import {
  faEye,
  faUser,
  faUserLock,
  faEyeSlash,
  faLock,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import "../styles/modal_user_process.css";

import Axios from "axios";
import "../styles/userTable.css";
import React, { useState, useEffect } from "react";
import "../styles/modal_user_process.css";
// import UserInfoTabs from "./modal_user_process";
import { Animated } from "react-animated-css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserShield } from "@fortawesome/free-solid-svg-icons";
import PswValidation from "./pswValidation";
import $ from "jquery";
function Modaluser(props) {
  const [dataList2, setdataList2] = useState([]);
  const [roleList, setroleList] = useState([]);
  const [channelList, setchannelList] = useState([]);
  const [value, setValue] = useState([]);
  const [pswVisible, setPswVisible] = React.useState(false);
  const [userData, setuserData] = useState([]);
  const [select, setSelect] = React.useState();
  const [select2, setSelect2] = React.useState();
  const [status, setstatus] = useState("");
  const [channel, setChannel] = useState("");
  
 
  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];

  const PswShowHide = () => {
    setPswVisible(!pswVisible);
  };

  useEffect(() => {
    Axios.get("http://localhost:3001/getrole").then((response) => {
      setdataList2(response.data);
    });
  }, []);
  useEffect(() => {
    Axios.post("http://localhost:3001/getUserRole/", {
      idVal: props.data,
    }).then((resultt) => {
      setroleList(resultt.data);
      // console.log("test" + roleList);
    });
  }, []);

  useEffect(() => {
    const map = Array.prototype.map;
    let newName = [];
    newName = map.call(roleList, (eachLetter) => {
      return eachLetter.role_id;
    });
    setValue(newName);
    // console.log(value);
  }, [roleList]);

  useEffect(() => {
    Axios.get("http://localhost:3001/getchannel").then((response) => {
      setchannelList(response.data);
    });

    Axios.post("http://localhost:3001/getdataUser/", {
      idVal: props.data,
    }).then((result) => {
      setuserData(result.data);
    });
  }, []);

  // **   if valid do this............................
  const valid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = "1";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "1";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "0";
  };

  // **   if invalid do this............................

  const invalid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = ".5";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "0";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "1";
  };

  const checkValidate = (e) => {
    const password = e;
    if (password.match(/[A-Z]/) != null) {
      valid("capital", "fa_check", "fa_times");
    } else {
      invalid("capital", "fa_check", "fa_times");
    }
    if (password.match(/[a-z]/) != null) {
      valid("lower", "fa_check", "fa_times");
    } else {
      invalid("lower", "fa_check", "fa_times");
    }

    if (password.match(/[0-9]/) != null) {
      valid("numbers", "fa_check", "fa_times");
    } else {
      invalid("numbers", "fa_check", "fa_times");
    }

    if (password.match(/[!@#$%^&*]/) != null) {
      valid("chars", "fa_check", "fa_times");
    } else {
      invalid("chars", "fa_check", "fa_times");
    }

    if (password.length > 7) {
      valid("more8", "fa_check", "fa_times");
    } else {
      invalid("more8", "fa_check", "fa_times");
    }
  };

  const resetBtn = () => {
    setSelect(null);
    setSelect2(null);
   
    setValue([]);
    $('input[name="inputFields"]').val("");
  };

  const updateUserBtn = () => {
    // console.log(role);
    let rolIDs = [];
    rolIDs = $('input[name="checkboxC"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();
    // setRole(searchIDs);
    console.log(rolIDs);

    Axios.post("http://localhost:3001/updateUser", {
      fn: $("#fnVal").val(),
      ln: $("#lnVal").val(),
      status: $("#statusInput").val(),
      channel: $("#channelInput").val(),
      email: $("#emailVal").val(),
      password: $("#pswVal").val(),
      role: value,
      idVal: props.data,
    }).then(() => {
      console.log("update success");
    });
  };

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <h4>
            <FontAwesomeIcon icon={faUserShield} /> User Info
          </h4>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* <PanelGroup> */}
        <Panel header="" shaded className="panel1">
          <Animated
            animationIn="zoomInLeft"
            animationOut="fadeOut"
            isVisible={true}
          >
            <Grid fluid="true">
              {userData.map((val, key) => {
                return (
                  <React.Fragment>
                    <Row className="show-grid">
                      <Col xs={12} xsPush={12} md={12}>
                        <label>Email:</label>{" "}
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon icon={faEnvelope} />
                          </InputGroup.Addon>

                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              id="emailVal"
                              placeholder="Email@abc.com"
                              type="text"
                              name="inputFields"
                              defaultValue={val.email}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Password:</label>
                        <InputGroup inside className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon icon={faLock}></FontAwesomeIcon>
                          </InputGroup.Addon>
                          <Whisper
                            trigger="focus"
                            speaker={
                              <Tooltip>
                                <PswValidation />
                              </Tooltip>
                            }
                          >
                            <Input
                              type={pswVisible ? "text" : "password"}
                              placeholder="Password"
                              className="pswField"
                              name="inputFields"
                              id="pswVal"
                              onChange={checkValidate}
                              defaultValue={val.password}
                            />
                          </Whisper>
                          <InputGroup.Button onClick={PswShowHide}>
                            {pswVisible ? (
                              <FontAwesomeIcon icon={faEye} />
                            ) : (
                              <FontAwesomeIcon icon={faEyeSlash} />
                            )}
                          </InputGroup.Button>
                        </InputGroup>
                        <label>Status:</label>
                        <Form.Group controlId="radioList">
                          {/* <label className="StatusGroup">Status:</label> */}
                          <input
                            type="text"
                            id="statusInput"
                            style={{
                              visibility: "hidden",
                              position: "absolute",
                            }}
                            value={status === "" ? val.status : status}
                          />
                          <SelectPicker
                            data={options}
                            value={select}
                            id="statusVal"
                            appearance="default"
                            placeholder="Status"
                            searchable={false}
                            defaultValue={
                              val.status === "Active" ? "Active" : "Inactive"
                            }
                            onChange={(event) => {
                              setstatus(event);
                              setSelect();
                            }}
                            style={{ width: 300 }}
                          />
                        </Form.Group>
                      </Col>

                      <Col xs={12} xsPull={12}>
                        <label>Name:</label>
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                          </InputGroup.Addon>
                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              style={{ width: 200 }}
                              placeholder="Name"
                              type="text"
                              id="fnVal"
                              name="inputFields"
                              defaultValue={val.fn}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Username:</label>
                        <InputGroup className="InputGroup">
                          <InputGroup.Addon>
                            <FontAwesomeIcon
                              icon={faUserLock}
                            ></FontAwesomeIcon>
                          </InputGroup.Addon>
                          <Whisper
                            trigger="hover"
                            speaker={<Tooltip>Required</Tooltip>}
                          >
                            <Input
                              style={{ width: 200 }}
                              placeholder="Username"
                              type="text"
                              id="lnVal"
                              name="inputFields"
                              defaultValue={val.ln}
                            />
                          </Whisper>
                        </InputGroup>
                        <label>Assigned Channel(s):</label>
                        <Form.Group controlId="channel">
                          <input
                            type="text"
                            style={{
                              visibility: "hidden",
                              position: "absolute",
                            }}
                            id="channelInput"
                            value={channel === "" ? val.idchannel : channel}
                          />

                          <SelectPicker
                            className="SelectPicker"
                            placeholder="Select Channel"
                            value={select2}
                            id="channelVal"
                            onChange={(event) => {
                              setChannel(event);
                              setSelect2();
                            }}
                            defaultValue={val.idchannel}
                            data={channelList.map((x) => {
                              return {
                                label: x.channel_name,
                                value: x.idchannel,
                              };
                            })}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                    <Row className="show-grid mt-2">
                      <Col>
                        <label>Assigned role(s):</label>
                        <Form.Group controlId="channel">
                          <CheckPicker
                            sticky
                            data={dataList2.map((x) => {
                              return {
                                label: x.role_name,
                                value: x.role_id,
                              };
                            })}
                            placement="rightEnd"
                            placeholder="Assign role(s) to user"
                            onChange={setValue}
                            value={value.map((val, kk) => val)}
                            style={{ width: 510 }}
                          />
                        </Form.Group>
                      </Col>
                    </Row>
                  </React.Fragment>
                );
              })}
            </Grid>
          </Animated>
        </Panel>
        {/*           
        </PanelGroup> */}
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
   
        <input
          type="button"
          value="Update"
          onClick={updateUserBtn}
          className="button-61 marginRIght"
        />
        <input
          type="button"
          value="Reset"
          className="Cancelbtn marginRIght"
          onClick={resetBtn}
        />
      </Modal.Footer>
    </React.Fragment>
  );
}

export default Modaluser;
