import * as RB from "react-bootstrap";
import React from "react";
import "../styles/card.css";
import { Animated } from "react-animated-css";

function Card(prop) {
  return (
    <React.Fragment>
      <Animated
        animationIn="fadeInRightBig"
        animationOut="fadeOut"
        isVisible={true}
      >
        <RB.Form className="mainform">{prop.children}</RB.Form>
      </Animated>
    </React.Fragment>
  );
}

export default Card;
