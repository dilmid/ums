// import React from "react";
import {
  Modal,
  PanelGroup,
  Panel,
  Table,
  Pagination,
  Checkbox,
  Grid,
  Row,
  Tooltip,
  Input,
  Whisper,
  Col,
  CheckboxGroup,
  InputGroup,
  SelectPicker,
  Form,
} from "rsuite";

import {
  faEye,
  faUser,
  faUserLock,
  faEyeSlash,
  faLock,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import "../styles/modal_user_process.css";

import Axios from "axios";
import "../styles/userTable.css";
import React, { useState, useEffect } from "react";
import "../styles/modal_user_process.css";
// import UserInfoTabs from "./modal_user_process";
import { Animated } from "react-animated-css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserShield, faSearch } from "@fortawesome/free-solid-svg-icons";
import PswValidation from "./pswValidation";
import $ from "jquery";
function ModaluserNew() {
  // const [loading, setLoading] = React.useState(false);
  const loading = false;
  const [limit, setLimit] = React.useState(10);
  const [page, setPage] = React.useState(1);
  const [dataList2, setdataList2] = useState([]);
  const [filteredResults, setFilteredResults] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [fn, setfn] = useState("");
  const [ln, setln] = useState("");
  const [email, setemail] = useState("");
  const [password, setpassword] = useState("");
  const [status, setstatus] = useState("");
  const [channel, setChannel] = useState("");
  const [rolestatus, setRolestatus] = useState([]);
  const [select, setSelect] = React.useState(null);
  const [select2, setSelect2] = React.useState(null);
  const handleChangeLimit = (dataKey) => {
    setPage(1);
    setLimit(dataKey);
  };

  useEffect(() => {
    Axios.get("http://localhost:3001/getrole").then((response) => {
      setdataList2(response.data);
    });
  }, []);

  const data = dataList2.filter((v, i) => {
    const start = limit * (page - 1);
    const end = start + limit;
    return i >= start && i < end;
  });
  const [channelList, setchannelList] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3001/getchannel").then((response) => {
      setchannelList(response.data);
    });
  }, []);

  const [pswVisible, setPswVisible] = React.useState(false);

  const PswShowHide = () => {
    setPswVisible(!pswVisible);
  };

  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];
  const searchItems = (e) => {
    setSearchInput(e);

    // console.log(searchInput);

    if (searchInput !== "") {
      const filteredData = data.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(data);
    }
  };
  // **   if valid do this............................
  const valid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = "1";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "1";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "0";
  };

  // **   if invalid do this............................

  const invalid = (item, v_icon, inv_icon) => {
    let text = document.querySelector(`#${item}`);
    text.style.opacity = ".5";

    let valid_icon = document.querySelector(`#${item} .${v_icon}`);
    valid_icon.style.opacity = "0";

    let invalid_icon = document.querySelector(`#${item} .${inv_icon}`);
    invalid_icon.style.opacity = "1";
  };

  const checkValidate = (e) => {
    const password = e;
    setpassword(e);
    if (password.match(/[A-Z]/) != null) {
      valid("capital", "fa_check", "fa_times");
    } else {
      invalid("capital", "fa_check", "fa_times");
    }
    if (password.match(/[a-z]/) != null) {
      valid("lower", "fa_check", "fa_times");
    } else {
      invalid("lower", "fa_check", "fa_times");
    }

    if (password.match(/[0-9]/) != null) {
      valid("numbers", "fa_check", "fa_times");
    } else {
      invalid("numbers", "fa_check", "fa_times");
    }

    if (password.match(/[!@#$%^&*]/) != null) {
      valid("chars", "fa_check", "fa_times");
    } else {
      invalid("chars", "fa_check", "fa_times");
    }

    if (password.length > 7) {
      valid("more8", "fa_check", "fa_times");
    } else {
      invalid("more8", "fa_check", "fa_times");
    }
  };

  const newUserBtn = () => {
    // console.log(role);
    let rolIDs = [];
    rolIDs = $('input[name="checkboxC"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();
    // setRole(searchIDs);
    console.log(rolIDs);

    Axios.post("http://localhost:3001/insertUser", {
      fn: fn,
      ln: ln,
      status: status,
      channel: channel,
      email: email,
      password: password,
      role: rolIDs,
    }).then(() => {
      console.log("success");
    });
  };

  const resetBtn = () => {
    setSelect(null);
    setSelect2(null);
    setRolestatus([]);
    $('input[name="inputFields"]').val("");
  };

const tableStyles = {
  header: {
    backgroundColor: "#079577",
    letterSpacing: "1.5px",
    fontSize: "14px",
    fontWeight: "700",
    color: "white",
    opacity: "0.8",
    textShadow: "2px 4px 3px rgba(0,0,0,0.3)",
  },
  borderLeft: {
    borderRadius: "5px 0px 0px 0px",
  },
  borderRight: {
    borderRadius: "0px 5px 0px 0px",
  },
};
  const handleChange = (rolestatus) => setRolestatus(rolestatus);

  return (
    <React.Fragment>
      <Modal.Header>
        <Modal.Title>
          <h4>
            <FontAwesomeIcon icon={faUserShield} /> New User Info
          </h4>

          <span className="tagline">Fill out the below form.</span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <PanelGroup id="panelGroup">
          <Panel header="" shaded className="panel1">
            <Grid fluid="true">
              <Row className="show-grid">
                <Col xs={12} xsPush={12} md={12}>
                  <InputGroup className="InputGroup">
                    <InputGroup.Addon>
                      <FontAwesomeIcon icon={faEnvelope} />
                    </InputGroup.Addon>

                    <Whisper
                      trigger="focus"
                      speaker={<Tooltip>Required</Tooltip>}
                    >
                      <Input
                        onChange={(event) => {
                          setemail(event);
                        }}
                        placeholder="Email@abc.com"
                        type="email"
                        name="inputFields"
                      />
                    </Whisper>
                  </InputGroup>

                  <InputGroup inside className="InputGroup">
                    <InputGroup.Addon>
                      <FontAwesomeIcon icon={faLock}></FontAwesomeIcon>
                    </InputGroup.Addon>
                    <Whisper
                      trigger="focus"
                      speaker={
                        <Tooltip>
                          <PswValidation />
                        </Tooltip>
                      }
                    >
                      <Input
                        type={pswVisible ? "text" : "password"}
                        placeholder="Password"
                        className="pswField"
                        name="inputFields"
                        onChange={checkValidate}
                      />
                    </Whisper>
                    <InputGroup.Button onClick={PswShowHide}>
                      {pswVisible ? (
                        <FontAwesomeIcon icon={faEye} />
                      ) : (
                        <FontAwesomeIcon icon={faEyeSlash} />
                      )}
                    </InputGroup.Button>
                  </InputGroup>

                  <Form.Group controlId="radioList">
                    {/* <label className="StatusGroup">Status:</label> */}

                    <SelectPicker
                      data={options}
                      value={select}
                      appearance="default"
                      id="statusSelect"
                      placeholder="Status"
                      searchable={false}
                      onChange={(event) => {
                        setstatus(event);
                        setSelect();
                      }}
                      style={{ width: 300 }}
                    />
                  </Form.Group>
                </Col>

                <Col xs={12} xsPull={12}>
                  <InputGroup className="InputGroup">
                    <InputGroup.Addon>
                      <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                    </InputGroup.Addon>
                    <Whisper
                      trigger="focus"
                      speaker={<Tooltip>Required</Tooltip>}
                    >
                      <Input
                        style={{ width: 200 }}
                        placeholder="Name"
                        name="inputFields"
                        type="text"
                        onChange={(event) => {
                          setfn(event);
                        }}
                      />
                    </Whisper>
                  </InputGroup>

                  <InputGroup className="InputGroup">
                    <InputGroup.Addon>
                      <FontAwesomeIcon icon={faUserLock}></FontAwesomeIcon>
                    </InputGroup.Addon>
                    <Whisper
                      trigger="focus"
                      speaker={<Tooltip>Required</Tooltip>}
                    >
                      <Input
                        style={{ width: 200 }}
                        placeholder="Username"
                        type="text"
                        name="inputFields"
                        onChange={(event) => {
                          setln(event);
                        }}
                      />
                    </Whisper>
                  </InputGroup>

                  <SelectPicker
                    className="SelectPicker"
                    placeholder="Select Channel"
                    value={select2}
                    onChange={(event) => {
                      setChannel(event);
                      setSelect2();
                    }}
                    data={channelList.map((x) => {
                      return {
                        label: x.channel_name,
                        value: x.idchannel,
                      };
                    })}
                  />
                </Col>
              </Row>
            </Grid>
          </Panel>

          <Panel
            id="panel2"
            header={
              <span>
                <FontAwesomeIcon
                  icon={faUserShield}
                  style={{
                    marginRight: "10px",
                    fontSize: "16px",
                    fontWeight: "bold",
                    textShadow: "6px 6px 0px rgba(0,0,0,0.2)",
                  }}
                />
                Assign role(s) to user
              </span>
            }
            shaded
            className="panel1"
          >
            <Grid fluid="true">
              <Row className="show-grid">
                <Col xs={3}> </Col>
                <Col xs={9} xsPush={12}>
                  <InputGroup>
                    <Input
                      onChange={searchItems}
                      id="searchRole"
                      style={{ color: "black" }}
                      placeholder="Search here"
                    />
                    <InputGroup.Addon>
                      <FontAwesomeIcon icon={faSearch} />
                    </InputGroup.Addon>
                  </InputGroup>
                </Col>
              </Row>
            </Grid>
            <Animated
              animationIn="fadeIn"
              animationOut="fadeOut"
              isVisible={true}
            >
              <div>
                <Table
                  id="table1"
                  height={350}
                  data={searchInput.length > 0 ? filteredResults : data}
                  loading={loading}
                  className="userTable"
                >
                  <Table.Column width={60} align="center" fixed>
                    <Table.HeaderCell
                     style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderLeft
                )}
                    >
                      #
                    </Table.HeaderCell>

                    <Table.Cell className="checkboxBorder" dataKey="role_id">
                      {(rowData) => {
                        return (
                          <CheckboxGroup
                            inline
                            name="checkboxList"
                            value={rolestatus}
                            onChange={handleChange}
                          >
                            <Checkbox
                              type="checkbox"
                              // onChange={checkBoxVal}
                              key={`${rowData.role_id}`}
                              name="checkboxC"
                              checked={rolestatus}
                              value={`${rowData.role_id}`}
                            >
                             
                            </Checkbox>
                          </CheckboxGroup>
                        );
                      }}
                    </Table.Cell>
                  </Table.Column>

                  <Table.Column width={135} fixed>
                    <Table.HeaderCell
                      style={Object.assign(
                  {},
                  tableStyles.header 
                )}
                    >
                      Role
                    </Table.HeaderCell>
                    <Table.Cell className="TableCell" dataKey="role_name" />
                  </Table.Column>

                  <Table.Column width={160}>
                    <Table.HeaderCell
                     style={Object.assign(
                  {},
                  tableStyles.header 
                )}
                    >
                      Created Date
                    </Table.HeaderCell>
                    <Table.Cell className="TableCell" dataKey="role_date" />
                  </Table.Column>

                  <Table.Column width={150}>
                    <Table.HeaderCell
                   style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderRight
                )}
                    >
                      Status
                    </Table.HeaderCell>
                    <Table.Cell className="TableCell" dataKey="role_status" />
                  </Table.Column>
                </Table>
                <div style={{ padding: 20 }}>
                  <Pagination
                    prev
                    next
                    first
                    last
                    ellipsis
                    boundaryLinks
                    maxButtons={5}
                    size="xs"
                    layout={["total", "-", "limit", "|", "pager", "skip"]}
                    total={dataList2.length}
                    limitOptions={[10, 20]}
                    limit={limit}
                    activePage={page}
                    onChangePage={setPage}
                    onChangeLimit={handleChangeLimit}
                  />
                </div>
              </div>
            </Animated>
          </Panel>
        </PanelGroup>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <input
          type="button"
          value="Submit"
          onClick={newUserBtn}
          className="button-61 marginRIght"
        />
        <input
          type="button"
          value="Reset"
          id="resetBtn"
          onClick={resetBtn}
          className="Cancelbtn marginRIght"
        />
      </Modal.Footer>
    </React.Fragment>
  );
}

export default ModaluserNew;
