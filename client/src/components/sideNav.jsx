import "../styles/sideNav.css";
import React from 'react';

import { Sidenav, Nav, Dropdown,Popover,Whisper } from "rsuite";
import "rsuite/dist/rsuite.min.css";
import { Link } from "react-router-dom";
import ArrowRightLineIcon from '@rsuite/icons/ArrowRightLine';
import { Peoples, OffRound,Dashboard} from "@rsuite/icons";
 

function SideNavBar() {
  
  const [activeKey, setActiveKey] = React.useState("1");


const renderSpeaker = ({ onClose, left, top, className, ...rest }, ref) => {
   const handleSelect = (eventKey) => {
    onClose();
    console.log(eventKey);
  };
  return (
    <Popover ref={ref} className={className} style={{ left, top, width:300,paddingLeft:'15px' }} full>
      <Dropdown.Menu onSelect={handleSelect}>
        
        <Dropdown.Item eventKey={3}>Sub Menu 1</Dropdown.Item>
        <Dropdown.Item eventKey={4}>Sub Menu 2</Dropdown.Item>
        <Dropdown.Item eventKey={5}>Sub Menu 3</Dropdown.Item>
        <Dropdown.Item eventKey={6}>Sub Menu 4</Dropdown.Item>
        <Dropdown.Item eventKey={7}>Sub Menu 5</Dropdown.Item>
      </Dropdown.Menu>
    </Popover>
  );
};

  return (
    <div className="sidebarContainer">
      <h3 className="sidebarHeading">UMS</h3>
      <hr />
      <Sidenav
        // defaultOpenKeys={["3", "4"]}
        activeKey={activeKey}
        onSelect={setActiveKey}
      >
        <Sidenav.Body  >
          <Nav>
            <Nav.Item eventKey="1" icon={<Dashboard />} className="theme navItem">
              <Link to="/dashboard" className="Link">
                Dashboard
              </Link>
            </Nav.Item>
            <Nav.Item eventKey="2" icon={<Peoples />} className="theme">
              <Link to="" className="Link">
                Menu
              </Link>
            </Nav.Item>

            <Nav.Item className="theme" eventKey="3" icon={<Peoples />}>
              <Whisper
                placement="rightEnd"
                trigger="click"
                speaker={renderSpeaker}
              >
                <div className="dropdownBox">
                  <span className="dropdown Link">Menu</span>

                  <ArrowRightLineIcon className="dropdown_arrow" />
                </div>
              </Whisper>
            </Nav.Item>

            <Nav.Item eventKey="4" icon={<Peoples />} className="theme">
              <Link to="/dashboard/user-Managment" className="Link">
                User Managment{" "}
              </Link>
            </Nav.Item>

            <Nav.Item eventKey="5" icon={<Peoples />} className="theme">
              <Link to="" className="Link">
                Menu
              </Link>
            </Nav.Item>
            <Nav.Item eventKey="6" icon={<Peoples />} className="theme">
              <Link to="" className="Link">
                Menu
              </Link>
            </Nav.Item>
            <Nav.Item eventKey="7" icon={<Peoples />} className="theme">
              <Link to="" className="Link">
                Menu
              </Link>
            </Nav.Item>
            <Nav.Item eventKey="8" icon={<Peoples />} className="theme">
              <Link to="" className="Link">
                Menu
              </Link>
            </Nav.Item>

            <Nav.Item eventKey="10" icon={<OffRound />} className="theme">
              <Link to="/" className="Link">
                Sign out
              </Link>
            </Nav.Item>
          </Nav>
        </Sidenav.Body>
      </Sidenav>
    </div>
  );
}

export default SideNavBar;
