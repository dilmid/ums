import React from "react";
import "../styles/searchbar.css";
import logo from "../images/logo.png";
import { Nav, Navbar, Input, InputGroup,Grid,
Row,
Col } from "rsuite";
import { Admin, Gear,Search } from "@rsuite/icons";
function SearchBar(prop) {

  return (
    <React.Fragment>
      <Navbar className="searchBar">
        <Grid fluid>
          <Row className="show-grid">
            <Col xs={24} sm={24} md={8} lg={6}>
              <Navbar.Brand href="#" className="searchBar_brand">
                <img
                  src={logo}
                  width="70"
                  height="70"
                  className="search_logo"
                  alt="logo"
                /> 
                <h2 >UMS</h2>
              </Navbar.Brand>
            </Col>
            <Col xs={24} sm={12} md={8} lg={12}>
              <Nav className="search_Bar_Nav">
                <InputGroup>
                  <Input className="search_Bar" placeholder="Seach here..." />
                  <InputGroup.Button>
                    <Search />
                  </InputGroup.Button>
                </InputGroup>
              </Nav>
            </Col>
            <Col xs={24} sm={12} md={8} lg={6}>
              <Nav pullRight>
                <Nav.Item icon={<Gear />}>Menu</Nav.Item>
                <Nav.Item icon={<Admin />}>Profile</Nav.Item>
              </Nav>
            </Col>
          </Row>
        </Grid>
      </Navbar>
    </React.Fragment>
  );
}

export default SearchBar;
