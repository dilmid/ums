import React, { useEffect } from "react";
import { MessageOutlined } from "@mui/icons-material";

import {
  Modal,
  Grid,
  Whisper,
  Tooltip,
  SelectPicker,
  Form,
  // Col,
  InputGroup,
  Input,
} from "rsuite";
import Axios from "axios";
import $ from "jquery";

export default function RoleUpdate(props) {
  const [status, setStatus] = React.useState();

  const [select, setSelect] = React.useState();
  const [txtarea, settxtarea] = React.useState();
  const [RoleData, setRoleData] = React.useState([]);
  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];

  useEffect(() => {
    Axios.post("http://localhost:3001/getdataRole/", {
      RoleidVal: props.data,
    }).then((result) => {
      setRoleData(result.data);
      console.log(RoleData);
    });
  }, []);

  const resetBtnRole = () => {
    setSelect(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };

  const roleUpdate = () => {
    Axios.post("http://localhost:3001/updateRole", {
      Rname: $("#roleName").val(),
      Rdesc: $("#roleInfo").val(),
      status: status,

      Rid: props.data,
    }).then(() => {
      console.log("update success");
    });
  };
  return (
    <React.Fragment>
      <Modal.Body>
        <Grid fluid faded className="roleModal">
          {RoleData.map((val, key) => {
            return (
              <React.Fragment>
                <InputGroup className="InputGroup">
                  <InputGroup.Addon>
                    <MessageOutlined />
                  </InputGroup.Addon>
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      style={{ width: 200 }}
                      placeholder="Role name"
                      type="text"
                      id="roleName"
                      name="inputFields"
                      defaultValue={val.role_name}
                    />
                  </Whisper>
                </InputGroup>

                <InputGroup className="InputGroup">
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      as="textarea"
                      name="inputFields"
                      rows={3}
                      defaultValue={val.role_desc}
                      value={txtarea}
                      id="roleInfo"
                      placeholder="Description"
                      onChange={(event) => {
                        settxtarea(event);
                      }}
                    />
                  </Whisper>
                </InputGroup>

                <Form.Group controlId="radioList">
                  <SelectPicker
                    data={options}
                    value={select}
                    block
                    id="statusVal"
                    appearance="default"
                    placeholder="Select Status"
                    searchable={false}
                    defaultValue={
                      val.role_status === "Active" ? "Active" : "Inactive"
                    }
                    onChange={(event) => {
                      setStatus(event);
                      setSelect();
                    }}
                    style={{ width: 600 }}
                  />
                </Form.Group>
              </React.Fragment>
            );
          })}
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <input
          type="button"
          value="Submit"
          onClick={roleUpdate}
          className="button-61 marginRIght"
        />
        <input
          type="button"
          value="Reset"
          className="Cancelbtn marginRIght"
          onClick={resetBtnRole}
        />
      </Modal.Footer>
    </React.Fragment>
  );
}
