import React, { useEffect } from "react";
import { MessageOutlined } from "@mui/icons-material";

import {
  Modal,
  Grid,
  Whisper,
  Tooltip,
  SelectPicker,
  Form,
  // Col,
  InputGroup,
  Input,
} from "rsuite";
import Axios from "axios";
import $ from "jquery";

export default function ChannelUpdate(props) {
  const [status, setStatus] = React.useState();

  const [select, setSelect] = React.useState();
  const [txtarea, settxtarea] = React.useState();
  const [ChannelData, setChannelData] = React.useState([]);
  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];

  useEffect(() => {
    Axios.post("http://localhost:3001/getdataChannel/", {
      ChannelidVal: props.data,
    }).then((result) => {
      setChannelData(result.data);
      console.log(ChannelData);
    });
  }, []);

  const resetBtnChannel = () => {
    setSelect(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };

  const channelUpdate = () => {
    Axios.post("http://localhost:3001/updateChannel", {
      Cname: $("#channelName").val(),
      Cdesc: $("#channelInfo").val(),
      status: $("#statusVal").val(),

      Cid: props.data,
    }).then(() => {
      console.log("update success");
    });
  };
  return (
    <React.Fragment>
      <Modal.Body>
        <Grid fluid faded className="roleModal">
          {ChannelData.map((val, key) => {
            return (
              <React.Fragment>
                <InputGroup className="InputGroup">
                  <InputGroup.Addon>
                    <MessageOutlined />
                  </InputGroup.Addon>
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      style={{ width: 200 }}
                      placeholder="Channel name"
                      type="text"
                      id="channelName"
                      name="inputFields"
                      defaultValue={val.channel_name}
                    />
                  </Whisper>
                </InputGroup>

                <InputGroup className="InputGroup">
                  <Whisper
                    trigger="focus"
                    speaker={<Tooltip>Required</Tooltip>}
                  >
                    <Input
                      as="textarea"
                      name="inputFields"
                      rows={3}
                      defaultValue={val.channel_desc}
                      value={txtarea}
                      id="channelInfo"
                      placeholder="Description"
                      onChange={(event) => {
                        settxtarea(event);
                      }}
                    />
                  </Whisper>
                </InputGroup>

                <Form.Group controlId="radioList">
                  <input
                    type="text"
                    id="statusVal"
                    defaultValue={
                      val.channel_status === "Active" ? "Active" : "Inactive"
                    }
                    style={
                      {
                        visibility: "hidden",
                        position: "absolute",
                      }
                    }
                    value={status === "" ? val.status : status}
                  />

                  <SelectPicker
                    data={options}
                    value={select}
                    block
                    appearance="default"
                    placeholder="Select Status"
                    searchable={false}
                    defaultValue={
                      val.channel_status === "Active" ? "Active" : "Inactive"
                    }
                    onChange={(event) => {
                      setStatus(event);
                      setSelect();
                    }}
                    style={{ width: 600 }}
                  />
                </Form.Group>
              </React.Fragment>
            );
          })}
        </Grid>
      </Modal.Body>
      <Modal.Footer className="btnGroupmodal">
        <input
          type="button"
          value="Submit"
          onClick={channelUpdate}
          className="button-61 marginRIght"
        />
        <input
          type="button"
          value="Reset"
          className="Cancelbtn marginRIght"
          onClick={resetBtnChannel}
        />
      </Modal.Footer>
    </React.Fragment>
  );
}
