import React, { useState, useRef, useEffect } from "react";
import {
  Panel,
  SelectPicker,
  Form,Modal,
  TagInput,Divider,Whisper,Tooltip,
  ButtonToolbar,
  CheckPicker,
  Button,
  Checkbox,
  PanelGroup,
  Row,
  Col,
  Grid,
} from "rsuite";
import "../styles/user_permissionTab.css";
import "../styles/button.css";
import Axios from "axios";
import {
  faUserShield,
  faUserTie,
  faEdit,
  faMagic,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PermissionUpdate from "../components/modal_PermissionUpdate";
function PermissionTab() {
  const [dataList, setdataList] = useState([]);
  const [pCategory, setpCategory] = useState([]);
  const [pName, setpName] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3001/getrole").then((response) => {
      setdataList(response.data);
    });
  }, []);

  useEffect(() => {
    Axios.get("http://localhost:3001/getpermission").then((response) => {
      setpCategory(response.data);
    });
  }, []);

  const showPname = (e) => {
    setcategoryVal(e);

    Axios.post("http://localhost:3001/getpermission_name", {
      pCategory: e,
    }).then((response) => {
      setpName(response.data);
    });
  };
  const footerStyles = {
    padding: "10px 2px",
    borderTop: "1px solid #e5e5e5",
  };

  const footerButtonStyle = {
    float: "right",
    marginRight: 10,
    marginTop: 2,
  };
  const allValue = pName.map((item) => item.permission_name);

  const picker = useRef();
  const [value, setValue] = useState([]);

  const handleChange = (value) => {
    setValue(value);
  };

  const handleCheckAll = (value, checked) => {
    setValue(checked ? allValue : []);
  };

  const [roleVal, setroleVal] = useState();
  const [categoryVal, setcategoryVal] = useState();
  const [newCategory, setnewCategory] = useState("");
  const [newPermission, setnewPermission] = useState([]);

  const resetBtn = () => {
    setroleVal([]);
    setcategoryVal([]);
    setValue([]);
  };
  const resetBtn2 = () => {
    setnewCategory("");
    setnewPermission([]);
  };
  const AssignPermission = () => {
    Axios.post("http://localhost:3001/assignPermission", {
      roleID: roleVal,
      PermissionID: value,
    }).then(() => {
      console.log("Assign permission success");
    });
  };
 const insertPermission = () => {
   
   Axios.post("http://localhost:3001/insertPermission", {
     newCategory: newCategory,
     newPermission: newPermission,
   }).then(() => {
     console.log("new permission insert success");
   });
 };
  
  
 
    const [open, setOpen] = React.useState(false);
    const permissionEdit = () => setOpen(true);
    const handleClose = () => setOpen(false);
  return (
    <React.Fragment>
      <Modal open={open} onClose={handleClose} size="sm">
        <Modal.Header>
          <Modal.Title style={{ fontWeight: "bold", letterSpacing: "0.5px" }}>
            <FontAwesomeIcon
              icon={faMagic}
              style={{
                marginRight: "10px",
                fontSize: "16px",
                fontWeight: "bold",
                textShadow: "6px 6px 0px rgba(0,0,0,0.2)",
              }}
            />
            Update Details
          </Modal.Title>
        </Modal.Header>
        <Divider />
        <PermissionUpdate />
      </Modal>

      <PanelGroup accordion shaded className="panelGroup">
        <Panel
          collapsible
          eventKey={1}
          id="panel1"
          header={
            <Grid fluid="true">
              <Row className="show-grid">
                <Col xs={11}>
                  <span style={{ display: "inline" }}>
                    <FontAwesomeIcon
                      icon={faUserTie}
                      style={{
                        marginRight: "10px",
                        fontSize: "16px",
                        fontWeight: "bold",
                        textShadow: "6px 6px 0px rgba(0,0,0,0.2)",
                      }}
                    />
                    <label
                      style={{
                        fontSize: "18px",
                        letterSpacing: "0.5px",
                        fontWeight: "bold",
                      }}
                    >
                      New Permission
                    </label>
                  </span>
                </Col>

                <Whisper
                  placement="left"
                  controlId="control-id-hover"
                  trigger="hover"
                  speaker={<Tooltip>Change permission</Tooltip>}
                >
                  <Col xs={1} xsPush={12}>
                    <FontAwesomeIcon icon={faEdit} onClick={permissionEdit} />
                  </Col>
                </Whisper>
              </Row>
            </Grid>
          }
        >
          <Grid fluid="true" className="permissionPanelGroup">
            <Row  className="permissionPanel">
              <Form fluid="true">
                <Form.Group controlId="name-1">
                  <Form.ControlLabel>Category</Form.ControlLabel>
                  <Form.Control
                    name="name"
                    placeholder="Add new category"
                    onChange={(event) => {
                      setnewCategory(event);
                    }}
                    value={newCategory}
                  />
                </Form.Group>
                <Form.Group controlId="email-1">
                  <Form.ControlLabel>Permission(s)</Form.ControlLabel>
                  <TagInput
                    block
                    value={newPermission}
                    trigger={["Enter", "Space", "Comma"]}
                    placeholder="Add new permission(s)"
                    onChange={(event) => {
                      setnewPermission(event);
                    }}
                  />
                </Form.Group>
              </Form>
            </Row>
            <Row className="show-grid">
              <Col xs={8} xsOffset={16}>
                <Form.Group>
                  <ButtonToolbar>
                    <div className="btnGroupmodal">
                      <input
                        type="button"
                        value="Submit"
                        onClick={insertPermission}
                        className="button-61 marginRIght"
                      />
                      <input
                        type="button"
                        value="Reset"
                        onClick={resetBtn2}
                        className="Cancelbtn marginRIght"
                      />
                    </div>
                  </ButtonToolbar>
                </Form.Group>
              </Col>
            </Row>
          </Grid>
        </Panel>
        <Panel
          collapsible
          header={
            <span style={{ display: "inline" }}>
              <FontAwesomeIcon
                icon={faUserShield}
                style={{
                  marginRight: "10px",
                  fontSize: "16px",
                  fontWeight: "bold",
                  textShadow: "6px 6px 0px rgba(0,0,0,0.2)",
                }}
              />
              <label
                style={{
                  fontSize: "18px",
                  letterSpacing: "0.5px",
                  fontWeight: "bold",
                }}
              >
                Assign Permission
              </label>
            </span>
          }
          eventKey={2}
          id="panel2"
        >
          <Grid fluid="true" className="permissionPanelGroup">
            <Row className="show-grid">
              <label className="pPanel1">Role : </label>
              <SelectPicker
                block
                value={roleVal}
                className="pPanel1 PselctPicker"
                placeholder="Select here"
                data={dataList.map((x) => {
                  return { label: x.role_name, value: x.role_id };
                })}
                onChange={(event) => {
                  setroleVal(event);
                }}
              />
            </Row>

            <Row  className="permissionPanel">
              <Col xsHidden xs={12}>
                <label className="pPanel1">Category :</label>
                <SelectPicker
                  data={pCategory.map((val) => {
                    return {
                      label: val.permission_category,
                      value: val.permission_category,
                    };
                  })}
                  style={{ width: 300 }}
                  onChange={showPname}
                  onSelect={showPname}
                  value={categoryVal}
                />
              </Col>
              <Col xs={12} >
                <label className="pPanel1 ml-5">Permission for :</label>

                <div className="example-item pPanel1">
                  <CheckPicker
                    data={pName.map((value) => {
                      return {
                        label: value.permission_name,
                        value: value.permission_type_id,
                      };
                    })}
                    placeholder="Select here"
                    ref={picker}
                    style={{ width: 250 }}
                    value={value}
                    onChange={handleChange}
                    renderExtraFooter={() => (
                      <div style={footerStyles}>
                        <Checkbox
                          inline
                          indeterminate={
                            value.length > 0 && value.length < allValue.length
                          }
                          checked={value.length === allValue.length}
                          onChange={handleCheckAll}
                        >
                          Check all
                        </Checkbox>

                        <Button
                          style={footerButtonStyle}
                          appearance="primary"
                          size="sm"
                          onClick={() => {
                            picker.current.close();
                          }}
                        >
                          Ok
                        </Button>
                      </div>
                    )}
                  />
                </div>
              </Col>
            </Row>
          </Grid>

          <Grid fluid>
            <Row className="show-grid">
              <Col xs={8} xsOffset={16}>
                <div className="btnGroupmodal">
                  <input
                    type="button"
                    value="Assign"
                    onClick={AssignPermission}
                    className="button-61 marginRIght"
                  />
                  <input
                    type="button"
                    value="Reset"
                    onClick={resetBtn}
                    className="Cancelbtn marginRIght"
                  />
                </div>
              </Col>
            </Row>
          </Grid>
        </Panel>
      </PanelGroup>
    </React.Fragment>
  );
}

export default PermissionTab;
