import { Button, Stack } from "@mui/material";
import PersonAddOutlinedIcon from "@mui/icons-material/PersonAddOutlined";
import { DeleteOutline, Search, MessageOutlined } from "@mui/icons-material";
import React, { useState,useEffect } from "react";
import {
  Table,
  Pagination,
  // Checkbox,
  Modal,
  Grid,
  Row,
  Col,
  InputGroup,
  Input,
  Tooltip,
  Whisper,
  // Radio,
  Form,
  // RadioGroup,
  SelectPicker,
} from "rsuite";
import Axios from "axios";
import { Animated } from "react-animated-css";
import "../styles/userTable.css";
import "../styles/button.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUserGraduate,
  faTrash,
  faUserEdit,
} from "@fortawesome/free-solid-svg-icons";
import $ from "jquery";
import ChannelUpdate from "../components/modal_channelUpdate";
import Swal from "sweetalert2";




function ChannelTab() {
  // const [loading, setLoading] = React.useState(false);
  const loading = false;
  const [limit, setLimit] = React.useState(10);
  const [page, setPage] = React.useState(1);
  const [dataList, setdataList] = useState([]);

  const handleChangeLimit = (dataKey) => {
    setPage(1);
    setLimit(dataKey);
  };
 
 useEffect(() => {
  Axios.get("http://localhost:3001/getchannel").then((response) => {
    setdataList(response.data);
  });
 }, []);


  const data = dataList.filter((v, i) => {
    const start = limit * (page - 1);
    const end = start + limit;
    return i >= start && i < end;
  });
  const [status, setStatus] = React.useState();

  const [open, setOpen] = React.useState(false);
  const [openUpdate, setOpenUpdate] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleClose2 = () => setOpenUpdate(false);
  const [select, setSelect] = React.useState();
  const [txtarea, settxtarea] = React.useState();
  const [filteredResults, setFilteredResults] = useState([]);
  const [searchInput, setSearchInput] = useState("");
 
  //  const [MultideleteID, setMultideleteID] = useState([]);

  

  const multiDeleteBtn = () => {
  let deleteID = [];
  deleteID = $('input[name="checkboxC"]:checked')
    .map(function () {
      return $(this).val();
    })
    .get();
  
    console.log(deleteID);
    if (deleteID.length === 0) {
      
      let timerInterval;
      Swal.fire({
        title: "Select Record(s)",
        html: "Please! select record(s) to delete.",
        timer: 3000,
        timerProgressBar: true,
        icon: "error",
        willClose: () => {
          clearInterval(timerInterval);
        },
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log("I was closed by the timer");
        }
      });
    } else {
      Swal.fire({
  title: "Are you sure?",
  text: "You won't be able to revert this!",
  icon: "warning",
  showCancelButton: true,
  confirmButtonColor: "#3085d6",
  cancelButtonColor: "#d33",
  confirmButtonText: "Yes, delete all!",
}).then((result) => {
  if (result.isConfirmed) {
    Axios.post("http://localhost:3001/deleteChannelMulti/", {
      deleteID: deleteID,
    }).then((result) => {
      //  setdeleteID(result.data);
    });
    Swal.fire("Deleted!", "Your Record has been deleted.", "success");
  }
});

    }


}

  const searchItems = (e) => {
    setSearchInput(e);

    // console.log(searchInput);

    if (searchInput !== "") {
      const filteredData = data.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(data);
    }
  };
  const options = [
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];

  const channelInsert = () => {
    Axios.post("http://localhost:3001/insertChannel", {
      channel: $("#channelName").val(),
      info: $("#channelInfo").val(),
      status: status,
    }).then(() => {
      console.log("Channel ssuccess");
    });
  };
  const [channelId, setchannelId] = useState({ data: "" });
  const updateChannelBtn = () => {
    $('input[name="Radios"]:checked').each(function () {
      console.log(this.value);
      setchannelId({ data: this.value });

      $(".radioImg").prop("checked", false);
      $(this).prop("checked", true);

      setOpen(false);
      setOpenUpdate(true);
    });
  };
  const resetBtnChannel = () => {
    setSelect(null);
    settxtarea("");
    $('input[name="inputFields"]').val("");
  };
const tableStyles = {
  header: {
    backgroundColor: "#079577",
    letterSpacing: "1.5px",
    fontSize: "14px",
    fontWeight: "700",
    color: "white",
    opacity: "0.8",
    textShadow: "2px 4px 3px rgba(0,0,0,0.3)",
  },
  borderLeft: {
    borderRadius: "5px 0px 0px 0px",
  },
  borderRight: {
    borderRadius: "0px 5px 0px 0px",
  },
};
  return (
    <React.Fragment>
      <div className="modal-container">
        <Modal size="sm" open={open} onClose={handleClose}>
          <Modal.Header>
            <Modal.Title>
              <h4>
                <FontAwesomeIcon icon={faUserGraduate} /> Channel Info
              </h4>

              <span className="tagline">Fill out the below form.</span>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid fluid faded className="roleModal">
              <InputGroup className="InputGroup">
                <InputGroup.Addon>
                  <MessageOutlined />
                </InputGroup.Addon>
                <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
                  <Input
                    style={{ width: 200 }}
                    placeholder="Channel name"
                    type="text"
                    id="channelName"
                    name="inputFields"
                  />
                </Whisper>
              </InputGroup>
              <InputGroup className="InputGroup">
                <Whisper trigger="focus" speaker={<Tooltip>Required</Tooltip>}>
                  <Input
                    as="textarea"
                    name="inputFields"
                    rows={3}
                    value={txtarea}
                    id="channelInfo"
                    placeholder="Description"
                    onChange={(event) => {
                      settxtarea(event);
                    }}
                  />
                </Whisper>
              </InputGroup>
              <Form.Group controlId="radioList">
                <SelectPicker
                  data={options}
                  value={select}
                  block
                  id="statusVal"
                  appearance="default"
                  placeholder="Select Status"
                  searchable={false}
                  // defaultValue={val.status === "Active" ? "Active" : "Inactive"}
                  onChange={(event) => {
                    setStatus(event);
                    setSelect();
                  }}
                  style={{ width: 600 }}
                />
              </Form.Group>
            </Grid>
          </Modal.Body>
          <Modal.Footer className="btnGroupmodal">
            <input
              type="button"
              value="Submit"
              onClick={channelInsert}
              className="button-61 marginRIght"
            />
            <input
              type="button"
              value="Reset"
              className="Cancelbtn marginRIght"
              onClick={resetBtnChannel}
            />
          </Modal.Footer>
        </Modal>

        <Modal size="sm" open={openUpdate} onClose={handleClose2}>
          <Modal.Header>
            <Modal.Title>
              <h4>
                <FontAwesomeIcon icon={faUserGraduate} /> Update Channel Info
              </h4>

              <span className="tagline">Fill out the below form.</span>
            </Modal.Title>
          </Modal.Header>
          <ChannelUpdate data={channelId.data} />
        </Modal>
      </div>

      <Grid fluid>
        <Row className="show-grid">
          <Col xs={6}>
            <Stack direction="row" spacing={2}>
              <Button
                variant="contained"
                className="createBtn"
                onClick={handleOpen}
                startIcon={<PersonAddOutlinedIcon />}
              >
                Create
              </Button>

              <Button
                variant="contained"
                className="removeBtn"
                startIcon={<DeleteOutline />}
                onClick={multiDeleteBtn}
              >
                Remove
              </Button>

              {/* <input type="button" value="click me" onClick={getData} /> */}
            </Stack>
          </Col>
          <Col xs={6} xsPush={12} inline>
            {/* <label htmlFor="search">Search by Task: </label> */}

            <InputGroup>
              <Input
                onChange={searchItems}
                id="search"
                style={{ color: "black" }}
                placeholder="Search here"
              />
              <InputGroup.Addon>
                <Search />
              </InputGroup.Addon>
            </InputGroup>

            {/* <input
              onChange={(e) => searchItems(e.target.value)}
              id="search"
              type="text"
              style={{ color: "black" }}
              placeholder="Search by name"
            /> */}
          </Col>
        </Row>
      </Grid>

      <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
        <div>
          <Table
            height={350}
            data={searchInput.length > 0 ? filteredResults : data}
            loading={loading}
            className="userTable"
          >
            <Table.Column width={60} align="center" fixed>
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderLeft
                )}
              >
                #
              </Table.HeaderCell>

              <Table.Cell className="checkboxBorder" dataKey="idchannel">
                {/* {(rowData) => {
                    return (
                      <Checkbox
                        key={`${rowData.channel_id}`}
                        name="checkboxC"
                      
                        value={`${rowData.channel_id}`}
                      >
                        {rowData.channel_id}
                      </Checkbox>
                    );
                  }} */}
              </Table.Cell>
            </Table.Column>

            <Table.Column width={150} fixed>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Channel
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="channel_name" />
            </Table.Column>
            <Table.Column width={200}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Description
              </Table.HeaderCell>

              <Table.Cell className="TableCell" dataKey="channel_desc" />
            </Table.Column>

            <Table.Column width={250}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Created Date
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="channel_date" />
            </Table.Column>

            <Table.Column width={200}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Status
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="channel_status" />
            </Table.Column>
            <Table.Column width={150} fixed="right">
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderRight
                )}
              >
                Action
              </Table.HeaderCell>

              <Table.Cell className="TableCell">
                {(rowData) => {
                  return (
                    <React.Fragment>
                      <Whisper
                        placement="leftStart"
                        controlId="control-id-hover"
                        trigger="hover"
                        speaker={<Tooltip>Edit</Tooltip>}
                      >
                        <span className="Action">
                          <input
                            type="Radio"
                            onClick={updateChannelBtn}
                            className="radioImg"
                            name="Radios"
                            value={`${rowData.idchannel}`}
                          />
                          <FontAwesomeIcon
                            icon={faUserEdit}
                            className="fontIcon"
                          />
                        </span>
                      </Whisper>
                      |
                      <Whisper
                        placement="rightStart"
                        controlId="control-id-hover"
                        trigger="hover"
                        speaker={<Tooltip>Remove</Tooltip>}
                      >
                        <span className="ActionRemove">
                          {/* <input
                            type="Radio"
                            onClick={RemoveUserBtn}
                            className="radioImg"
                            name="Radios"
                            value={`${rowData.role_id}`}
                          />
                          <FontAwesomeIcon
                            icon={faTrash}
                            className="fontIcon RemoveIcon"
                          />
                         */}
                          <input
                            name="checkboxC"
                            value={`${rowData.idchannel}`}
                            type="checkbox"
                            id={`${rowData.idchannel}`}
                            className="radioImg2"
                          />
                          <label
                            for={`${rowData.idchannel}`}
                            className="radioImg2Label"
                          >
                            <FontAwesomeIcon
                              icon={faTrash}
                              className="fontIcon RemoveIcon"
                            />
                          </label>
                        </span>
                      </Whisper>
                    </React.Fragment>
                  );
                }}
              </Table.Cell>
            </Table.Column>
          </Table>
          <div style={{ padding: 20 }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              maxButtons={5}
              size="xs"
              layout={["total", "-", "limit", "|", "pager", "skip"]}
              total={dataList.length}
              limitOptions={[10, 20]}
              limit={limit}
              activePage={page}
              onChangePage={setPage}
              onChangeLimit={handleChangeLimit}
            />
          </div>
        </div>
      </Animated>
    </React.Fragment>
  );
}

export default ChannelTab;
