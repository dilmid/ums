import { Breadcrumb } from "rsuite";
import "../styles/UserManagment.css";
import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import UserTab from "./user_userTab";
import RoleTab from "./user_roleTab";
import PermissionTab from "./user_permissionTab";
import ChannelTab from "./user_channelTab";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <React.Fragment>
      <Breadcrumb className="Breadcrumb">
        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
        <Breadcrumb.Item href="/components">Components</Breadcrumb.Item>
        <Breadcrumb.Item active>Breadcrumb</Breadcrumb.Item>
      </Breadcrumb>

      <Box sx={{ width: "100%" }} className="tabPanel">
        <Box
          sx={{ borderBottom: 1, borderColor: "divider" }}
          className="tabHead"
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
            className="tabColor"
          >
            <Tab label="User" {...a11yProps(0)} className="tabColor" />
            <Tab label="Role" {...a11yProps(1)} className="tabColor" />
            <Tab label="Channel" {...a11yProps(2)} className="tabColor" />
            <Tab label="Permission" {...a11yProps(3)} className="tabColor" />
          </Tabs>
        </Box>

        <TabPanel value={value} index={0}>
          <UserTab />
        </TabPanel>

        <TabPanel value={value} index={1}>
          <RoleTab />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <ChannelTab/>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <PermissionTab />
        </TabPanel>
      </Box>
    </React.Fragment>
  );
}
