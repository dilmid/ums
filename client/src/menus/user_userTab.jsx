import React, { useState, useEffect } from "react";
import { Button, Stack } from "@mui/material";
import PersonAddOutlinedIcon from "@mui/icons-material/PersonAddOutlined";
import { DeleteOutline, Search } from "@mui/icons-material";
import { Animated } from "react-animated-css";
import {
  Table,
  Pagination,
  // Checkbox,
  Modal,
  Grid,
  Whisper,
  Tooltip,
  Row,
  // Radio,
  Col,
  InputGroup,
  Input,
} from "rsuite";
import "../styles/button.css";
import Axios from "axios";
import "../styles/userTable.css";
import ModalUser from "../components/modal_user";
import $ from "jquery";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faUserEdit } from "@fortawesome/free-solid-svg-icons";
import ModaluserNew from "../components/modal_userNew";
import Swal from "sweetalert2";

function UserTab() {
 
  const loading = false;
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);
  const [dataList, setdataList] = useState([]);
  const [userData, setuserData] = useState({ data: "" });
  const [filteredResults, setFilteredResults] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [modalUserUpdate, setmodalUserUpdate] = useState(false);
  const [modalUserNew, setmodalUserNew] = useState(false);

  const data = dataList.filter((v, i) => {
    const start = limit * (page - 1);
    const end = start + limit;
    return i >= start && i < end;
  });

  const handleChangeLimit = (dataKey) => {
    setPage(1);
    setLimit(dataKey);
  };

  useEffect(() => {
    Axios.get("http://localhost:3001/getdata").then((response) => {
      setdataList(response.data);
    });
  }, []);

  const handleClose1 = () => setmodalUserNew(false);
  const handleClose2 = () => setmodalUserUpdate(false);
  // const [APIData, setAPIData] = useState([]);

  const searchItems = (e) => {
    setSearchInput(e);

    // console.log(searchInput);

    if (searchInput !== "") {
      const filteredData = data.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(data);
    }
  };

  const openNewUSerBtn = () => {
    setmodalUserNew(true);
    setmodalUserUpdate(false);
  };

  const updateUserBtn = () => {
    $('input[name="Radios"]:checked').each(function () {
      console.log(this.value);
      setuserData({ data: this.value });

      $(".radioImg").prop("checked", false);
      $(this).prop("checked", true);

      // setModalOpen(true);
      setmodalUserNew(false);
      setmodalUserUpdate(true);
    });
  };

 

  const multiDeleteBtn = () => {
    let deleteID = [];
    deleteID = $('input[name="checkboxC"]:checked')
      .map(function () {
        return $(this).val();
      })
      .get();
    // setRole(searchIDs);
    console.log(deleteID);
    if (deleteID.length === 0) {
      let timerInterval;
      Swal.fire({
        title: "Select Record(s)",
        html: "Please! select record(s) to delete.",
        timer: 3000,
        timerProgressBar: true,
        icon: "error",
        willClose: () => {
          clearInterval(timerInterval);
        },
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log("I was closed by the timer");
        }
      });
    } else {
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#079577",
        cancelButtonColor: "#fd7e14",

        confirmButtonText: "Yes, delete all!",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post("http://localhost:3001/deleteUserMulti/", {
            deleteID: deleteID,
          }).then((result) => {
            //  setdeleteID(result.data);
          });
          Swal.fire("Deleted!", "Your Record has been deleted.", "success");
        }
      });
    }
  };
 
const tableStyles = {
  header: {
    backgroundColor: "#079577",
    letterSpacing: "1.5px",
    fontSize: "14px",
    fontWeight: "700",
    color: "white",
    opacity: "0.8",
    textShadow: "2px 4px 3px rgba(0,0,0,0.3)",
  },
  borderLeft: {
    borderRadius: "5px 0px 0px 0px",
  },
  borderRight: {
    borderRadius: "0px 5px 0px 0px",
  },
};
  return (
    <React.Fragment>
      <div className="modal-container">
        <Modal size="sm" open={modalUserNew} onClose={handleClose1}>
          <ModaluserNew />
        </Modal>

        <Modal size="sm" open={modalUserUpdate} onClose={handleClose2}>
          <ModalUser data={userData.data} />
        </Modal>
      </div>

      <Grid fluid>
        <Row className="show-grid">
          <Col xs={6}>
            <Stack direction="row" spacing={2}>
              <Button
                variant="contained"
                className="createBtn"
                onClick={openNewUSerBtn}
                startIcon={<PersonAddOutlinedIcon />}
              >
                Create
              </Button>

              <Button
                variant="contained"
                className="removeBtn"
                startIcon={<DeleteOutline />}
                onClick={multiDeleteBtn}
              >
                Remove
              </Button>
            </Stack>
          </Col>
          <Col xs={6} xsPush={12} inline="true">
            {/* <label htmlFor="search">Search by Task: </label> */}

            <InputGroup>
              <Input
                onChange={searchItems}
                id="search"
                style={{ color: "black" }}
                placeholder="Search here"
              />
              <InputGroup.Addon>
                <Search />
              </InputGroup.Addon>
            </InputGroup>
          </Col>
        </Row>
      </Grid>

      <Animated animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
        <div>
          <Table
            height={350}
            data={searchInput.length > 0 ? filteredResults : data}
            loading={loading}
            className="userTable"
            id="datafilter"
          >
            <Table.Column width={60} align="center" fixed>
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderLeft
                )}
              >
                #
              </Table.HeaderCell>

              <Table.Cell className="checkboxBorder" dataKey="iduser">
                {/* {(rowData) => {
                    return (
                      <Checkbox
                        key={`${rowData.iduser}`}
                        name="checkboxC"
                        // onChange={handleChange}
                        value={`${rowData.iduser}`}
                      ></Checkbox>
                    );
                  }} */}
              </Table.Cell>
            </Table.Column>

            <Table.Column width={150} fixed>
              <Table.HeaderCell
                className="header__cell"
                style={Object.assign({}, tableStyles.header)}
              >
                First Name
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="fn" />
            </Table.Column>

            <Table.Column width={150}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Last Name
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="ln" />
            </Table.Column>

            <Table.Column width={150}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Email
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="email" />
            </Table.Column>
            <Table.Column width={100} flexGrow={1}>
              <Table.HeaderCell style={Object.assign({}, tableStyles.header)}>
                Status
              </Table.HeaderCell>
              <Table.Cell className="TableCell" dataKey="status" />
            </Table.Column>
            <Table.Column width={200} fixed="right">
              <Table.HeaderCell
                style={Object.assign(
                  {},
                  tableStyles.header,
                  tableStyles.borderRight
                )}
              >
                Action
              </Table.HeaderCell>

              <Table.Cell className="TableCell">
                {(rowData) => {
                  return (
                    <React.Fragment>
                      <Whisper
                        placement="leftStart"
                        controlId="control-id-hover"
                        trigger="hover"
                        speaker={<Tooltip>Edit</Tooltip>}
                      >
                        <span className="Action">
                          <input
                            type="Radio"
                            onClick={updateUserBtn}
                            className="radioImg"
                            name="Radios"
                            value={`${rowData.iduser}`}
                          />
                          <FontAwesomeIcon
                            icon={faUserEdit}
                            className="fontIcon"
                          />
                        </span>
                      </Whisper>
                      |
                      <Whisper
                        placement="rightStart"
                        controlId="control-id-hover"
                        trigger="hover"
                        speaker={<Tooltip>Remove</Tooltip>}
                      >
                        <span className="ActionRemove">
              
                          <input
                            name="checkboxC"
                            value={`${rowData.iduser}`}
                            type="checkbox"
                            id={`${rowData.iduser}`}
                            className="radioImg2"
                          />
                          <label
                            for={`${rowData.iduser}`}
                            className="radioImg2Label"
                          >
                            <FontAwesomeIcon
                              icon={faTrash}
                              className="fontIcon RemoveIcon"
                            />
                          </label>
                        </span>
                      </Whisper>
                    </React.Fragment>
                  );
                }}
              </Table.Cell>
            </Table.Column>
          </Table>
          <div style={{ padding: 20 }}>
            <Pagination
              prev
              next
              first
              last
              ellipsis
              boundaryLinks
              maxButtons={5}
              size="xs"
              layout={["total", "-", "limit", "|", "pager", "skip"]}
              total={dataList.length}
              limitOptions={[10, 20]}
              limit={limit}
              activePage={page}
              onChangePage={setPage}
              onChangeLimit={handleChangeLimit}
            />
          </div>
        </div>
      </Animated>
    </React.Fragment>
  );
}

export default UserTab;
