import Signin from "./pages/signin";
import { Route, Switch } from "react-router-dom";
import React from "react";
import ResetPassword from "./pages/resetPsw";
import ResetPassword2 from "./pages/resetPsw2";
import Dashboard from "./pages/dashboard";
// import UserManagment from "./components/UserManagment";

function App() {
  return (
    <div className="App">
      <React.Fragment>
        <Switch>
          <Route path={["/", "/signin"]} exact>
            <Signin />
          </Route>
          <Route path="/reset-password">
            <ResetPassword />
          </Route>
          <Route path="/reset-password2">
            <ResetPassword2 />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          {/* <Route path="/dashboard/user-Managment">
            <UserManagment />
          </Route> */}
        </Switch>
      </React.Fragment>
    </div>
  );
}

export default App;
