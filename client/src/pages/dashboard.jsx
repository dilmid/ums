import React from "react";
import SearchBar from "../components/searchbar";
import SideNavBar from "../components/sideNav";
import UserManagment from "../menus/tabPanel";
import "../styles/dashboard.css";
import { Container, Header, Content, Footer, Sidebar } from "rsuite";
import { Route, Switch } from "react-router-dom";

function Dashboard() {
  return (
    <React.Fragment>
      <Container fluid="true">
        <Header>
          <SearchBar />
        </Header>
        <Container fluid="true">
          <Sidebar>
            <SideNavBar />
          </Sidebar>
          <Content className="bashboard_body">
            <Switch>
              <Route path="/dashboard/user-Managment">
                <UserManagment />
              </Route>
              <Route path="/dashboard"></Route>
            </Switch>
          </Content>
        </Container>
        <Footer>Footer</Footer>
      </Container>
    </React.Fragment>
  );
}

export default Dashboard;
