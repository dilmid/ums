import Card from "../components/card";
import logo from "../images/logo.png";
import React from "react";
import * as RB from "react-bootstrap";
import { Link } from "react-router-dom";
import Textt from "../components/text";
import "../styles/resetPsw.css";
function ResetPassword() {
  return (
    <React.Fragment>
      <RB.Container fluid="true" className="reset_main_container">
        <RB.Row>
          <RB.Col>
            <Textt />
          </RB.Col>
          <RB.Col xs={5}>
            <Card>
              {/* <RB.Form className="mainform"> */}
              <RB.Form.Group className="reset_heading">
                <img src={logo} alt="logo" className="reset_logo" />

                <h1 className="defaultt">Reset Password</h1>
              </RB.Form.Group>

              <RB.Form.Group className="mb-3" controlId="formBasicEmail">
                <RB.Form.Label className="defaultt">
                  Email address
                </RB.Form.Label>
                <RB.Form.Control
                  type="email"
                  placeholder="Enter email"
                  className="inputt"
                />
                <RB.Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </RB.Form.Text>
              </RB.Form.Group>

              <RB.Form.Group>
                <Link to="/signin" className="reset_link">
                  SignIn?
                </Link>
              </RB.Form.Group>

              <RB.Form.Group>
                <input
                  className="button-61"
                  value="Request a Reset"
                  type="button"
                />
              </RB.Form.Group>
              {/* </RB.Form> */}
            </Card>
          </RB.Col>
        </RB.Row>
      </RB.Container>
    </React.Fragment>
  );
}

export default ResetPassword;
