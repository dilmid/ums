import React from "react";
import * as RB from "react-bootstrap";
import "../styles/signin.css";
import "../styles/button.css";
import { Link } from "react-router-dom";
import logo from "../images/logo.png";
import Textt from "../components/text";
import Card from "../components/card";

function Signin() {
  return (
    <React.Fragment>
      <RB.Container fluid="true" className="main_container">
        <RB.Row>
          <RB.Col>
            <Textt />
          </RB.Col>
          <RB.Col xs={5} className="coll">
            {/* <RB.Form className="mainform"> */}
            <Card>
              <RB.Form.Group className="heading">
                <img src={logo} alt="logo" className="logo" />

                <h1 className="defaultt">SIGN IN</h1>
              </RB.Form.Group>

              <RB.Form.Group className="mb-3" controlId="formBasicEmail1">
                <RB.Form.Label className="defaultt">
                  Email address
                </RB.Form.Label>
                <RB.Form.Control
                  type="email"
                  placeholder="Enter email"
                  className="inputt"
                />
                <RB.Form.Text className="text-muted">
                  We'll never share your email with anyone else.
                </RB.Form.Text>
              </RB.Form.Group>

              <RB.Form.Group className="mb-3" controlId="formBasicPassword">
                <RB.Form.Label className="defaultt">Password</RB.Form.Label>
                <RB.Form.Control
                  type="password"
                  placeholder="Password"
                  className="inputt"
                />
              </RB.Form.Group>
              <RB.Form.Group className="mb-3" controlId="formBasicCheckbox">
                <RB.Form.Check type="checkbox" label="Check me out" />
              </RB.Form.Group>
              <RB.Form.Group>
                <Link to="/reset-password" className="reset_link">
                  Forgot Password?
                </Link>
              </RB.Form.Group>

              <RB.Form.Group>
                <input className="button-61" value="Sign In" type="button" />
              </RB.Form.Group>
              {/* </RB.Form> */}
            </Card>
          </RB.Col>
        </RB.Row>
      </RB.Container>
    </React.Fragment>
  );
}

export default Signin;
